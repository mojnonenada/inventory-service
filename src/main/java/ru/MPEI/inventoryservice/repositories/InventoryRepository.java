package ru.MPEI.inventoryservice.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.MPEI.inventoryservice.models.Inventory;

import java.util.List;

public interface InventoryRepository extends JpaRepository<Inventory, Long> {
    List<Inventory> findBySkuCodeIn(List<String> skuCodes);
}
